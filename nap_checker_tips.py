import json
from pprint import pprint
import selenium
from dotenv import load_dotenv
import re
from time import sleep
from datetime import datetime, date, timedelta
import random
from gql import gql
import pytz
from dotenv import load_dotenv
import traceback
import requests
import socket
import os
dotenv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env')
load_dotenv(dotenv_path)
PROXY = os.environ.get("PROXY")
API_URL = os.environ.get("API_URL")
API_KEY = os.environ.get("API_KEY")

HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
}


def nap_checker_tips(self, task, url=None):

    def switchToTab():
        print("switching tab")
        self.drivers[self.tabs['nap_checker']['driver']
                     ]['driver'].switch_to.window(self.tabs['nap_checker']['handle'])
        refreshChecker = random.randint(1, 100)
        if refreshChecker >= 94:
            self.drivers[self.tabs['nap_checker']['driver']
                         ]['driver'].refresh()
            sleep(2)

    # get headers
    try:
        if task == "get_urls":
            if "nap_checker" not in self.tabs:
                print("opening")
                self.openURL(
                    "nap_checker", "https://www.napchecker.com/tipster.php", "standard")
            else:
                switchToTab()

            html = self.drivers[self.tabs['nap_checker']['driver']
                                ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

            sleep(2)
            # turn page into soup
            main_soup = self.makeSoup(html)

            race_links_table = main_soup.find('table', {'id': 'meetings'})

            # loop through races
            urls = []
            races = []
            for row in race_links_table.find_all('tr'):

                for td in row.find_all("td"):
                    link = td.find("a")
                    if link is None:

                        races.append(td.text)
                    else:
                        races.append(link.text)

            for race in races:
                if race == "":
                    continue
                self.drivers[self.tabs['nap_checker']['driver']
                             ]['driver'].get(f"https://www.napchecker.com/tipster.php?place={race}")

                race_times = []
                sleep(2)
                html = self.drivers[self.tabs['nap_checker']['driver']
                                    ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")
                race_soup = self.makeSoup(html)

                times = race_soup.find(
                    "table", {'id': "race-times"}).find_all("td")

                for t in times:
                    link = t.find("a")
                    if link is None:
                        if t.text == "":
                            continue

                        race_times.append(t.text)
                    else:
                        if link.text == "":
                            continue
                        race_times.append(link.text)

                for race_time in race_times:
                    urls.append(
                        f"https://www.napchecker.com/tipster.php?place={race}&time={race_time.replace(':', '%3A')}")

            return urls

        elif task == "get_data":
            if "nap_checker" not in self.tabs:
                print("opening")
                self.openURL(
                    "nap_checker", "https://www.napchecker.com/tipster.php", "standard")
            else:
                switchToTab()

            todays_date_string = date.today().strftime("%Y-%m-%d")

            if 'napchecker_trades' not in self.state:
                self.state['napchecker_trades'] = {}

            self.drivers[self.tabs['nap_checker']['driver']
                         ]['driver'].get(url)
            url_list = url.split("=")
            race = url_list[1].replace("&time", "")
            race_time = url_list[2].replace("%3A", ":")

            sleep(2)
            html = self.drivers[self.tabs['nap_checker']['driver']
                                ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")
            race_soup = self.makeSoup(html)

            tip_table_rows = race_soup.find(
                'div', {'id': 'naps-results'}).find("table").find_all("tr")
            runners = []
            fives = []
            fours = []
            threes = []
            for tip_row in tip_table_rows[4:]:

                if "class" not in tip_row or tip_row['class'] == "" or tip_row['class'] is None:
                    row_tds = tip_row.find_all("td")
                    print(row_tds)
                    try:
                        closed = row_tds[5].find("span")
                    except IndexError:
                        return
                    if closed is not None and closed.text == "CLSD":
                        return
                    runner_name = row_tds[2].text.strip()
                    no_of_tips = len(row_tds[4].find_all("a"))
                    runners.append(
                        {"runnerName": runner_name, "noOfTips": no_of_tips})
                    if len(row_tds[4].find_all("a")) >= 5:
                        fives.append(row_tds[2].text.strip())
                    elif len(row_tds[4].find_all("a")) == 4:
                        fours.append(row_tds[2].text.strip())
                    elif len(row_tds[4].find_all("a")) == 3:
                        threes.append(row_tds[2].text.strip())
            naive = datetime.now()
            timezone = pytz.timezone("Europe/London")
            aware1 = timezone.localize(naive)
            if len(fives) > 0 and len(threes) > 0:
                print("send")
                for five in fives:
                    if f'{race}-{five}' not in self.state['napchecker_trades']:
                        trade_query = f"""
                                mutation {{ newTrade(inputTradeAlert: {{
                                    strategy: "Nap Checker",
                                    selection: "{five}",
                                    sport: "Horse Racing",
                                    positionType: "Back",
                                    event: "{race}",
                                    starttime: {int(datetime.strptime(f"{todays_date_string} {race_time}", "%Y-%m-%d %H:%M").timestamp())  - int(aware1.utcoffset().total_seconds())},
                                }})}}
                            """
                        result = self.sendToApi(trade_query)
                        print(result)
                        if "errors" not in result:
                            self.state['napchecker_trades'][f'{race}-{five}'] = {
                                "strategy": "Nap Checker",
                                "selection": five,
                                "sport": "Horse Racing",
                                "positionType": "Back",
                                "event": race,
                                "starttime": int(datetime.strptime(f"{todays_date_string} {race_time}", "%Y-%m-%d %H:%M").timestamp()) - int(aware1.utcoffset().total_seconds())

                            }
                for four in fours:
                    if f'{race}-{four}' not in self.state['napchecker_trades']:
                        trade_query = f"""
                                mutation {{ newTrade(inputTradeAlert: {{
                                    strategy: "Nap Checker",
                                    selection: "{four}",
                                    sport: "Horse Racing",
                                    positionType: "Back",
                                    event: "{race}",
                                    starttime: {int(datetime.strptime(f"{todays_date_string} {race_time}", "%Y-%m-%d %H:%M").timestamp())  - int(aware1.utcoffset().total_seconds())},
                                }})}}
                            """
                        result = self.sendToApi(trade_query)
                        print(result)
                        if "errors" not in result:
                            self.state['napchecker_trades'][f'{race}-{four}'] = {
                                "strategy": "Nap Checker",
                                "selection": four,
                                "sport": "Horse Racing",
                                "positionType": "Back",
                                "event": race,
                                "starttime": int(datetime.strptime(f"{todays_date_string} {race_time}", "%Y-%m-%d %H:%M").timestamp()) - int(aware1.utcoffset().total_seconds())

                            }
                for three in threes:
                    if f'{race}-{three}' not in self.state['napchecker_trades']:
                        trade_query = f"""
                                mutation {{ newTrade(inputTradeAlert: {{
                                    strategy: "Nap Checker",
                                    selection: "{three}",
                                    sport: "Horse Racing",
                                    positionType: "Back",
                                    event: "{race}",
                                    starttime: {int(datetime.strptime(f"{todays_date_string} {race_time}", "%Y-%m-%d %H:%M").timestamp())  - int(aware1.utcoffset().total_seconds())},
                                }})}}
                            """
                        result = self.sendToApi(trade_query)
                        print(result)
                        if "errors" not in result:
                            self.state['napchecker_trades'][f'{race}-{three}'] = {
                                "strategy": "Nap Checker",
                                "selection": three,
                                "sport": "Horse Racing",
                                "positionType": "Back",
                                "event": race,
                                "starttime": int(datetime.strptime(f"{todays_date_string} {race_time}", "%Y-%m-%d %H:%M").timestamp()) - int(aware1.utcoffset().total_seconds())

                            }
            elif len(fours) > 1:

                for four in fours:
                    if f'{race}-{four}' not in self.state['napchecker_trades']:
                        trade_query = f"""
                                    mutation {{ newTrade(inputTradeAlert: {{
                                        strategy: "Nap Checker",
                                        selection: "{four}",
                                        sport: "Horse Racing",
                                        positionType: "Back",
                                        event: "{race}",
                                        starttime: {int(datetime.strptime(f"{todays_date_string} {race_time}", "%Y-%m-%d %H:%M").timestamp())  - int(aware1.utcoffset().total_seconds())},
                                    }})}}
                                """
                        result = self.sendToApi(trade_query)
                        print(result)
                    if "errors" not in result:
                        self.state['napchecker_trades'][f'{race}-{four}'] = {
                            "strategy": "Nap Checker",
                            "selection": four,
                            "sport": "Horse Racing",
                            "positionType": "Back",
                            "event": race,
                            "starttime": int(datetime.strptime(f"{todays_date_string} {race_time}", "%Y-%m-%d %H:%M").timestamp()) - int(aware1.utcoffset().total_seconds())

                        }
            elif len(fives) > 1:
                for five in fives:
                    if f'{race}-{five}' not in self.state['napchecker_trades']:
                        trade_query = f"""
                                mutation {{ newTrade(inputTradeAlert: {{
                                    strategy: "Nap Checker",
                                    selection: "{five}",
                                    sport: "Horse Racing",
                                    positionType: "Back",
                                    event: "{race}",
                                    starttime: {int(datetime.strptime(f"{todays_date_string} {race_time}", "%Y-%m-%d %H:%M").timestamp())  - int(aware1.utcoffset().total_seconds())},
                                }})}}
                            """
                        result = self.sendToApi(trade_query)
                        print(result)
                        if "errors" not in result:
                            self.state['napchecker_trades'][f'{race}-{five}'] = {
                                "strategy": "Nap Checker",
                                "selection": five,
                                "sport": "Horse Racing",
                                "positionType": "Back",
                                "event": race,
                                "starttime": int(datetime.strptime(f"{todays_date_string} {race_time}", "%Y-%m-%d %H:%M").timestamp()) - int(aware1.utcoffset().total_seconds())

                            }
            race_data = {
                "starttime": datetime.strptime(f"{todays_date_string} {race_time}", "%Y-%m-%d %H:%M").timestamp(),
                "course": race,
                "runners": runners
            }

            pprint(race_data)
            keys_to_delete = []
            for key, value in self.state['napchecker_trades'].items():
                if value['starttime'] < int((datetime.now() - timedelta(hours=1)).timestamp()):
                    keys_to_delete.append(key)
            for k in keys_to_delete:
                del self.state['napchecker_trades'][k]
            self.saveState()
            return
    except selenium.common.exceptions.WebDriverException:
        err = traceback.format_exc()
        if "disconnected from renderer" in err:
            self.quit()
            self.loadChromedriver(
                key='standard')
            self.loadChromedriver(key='proxyDriver', opts={
                'proxy': PROXY, 'vanila': True})
            self.tabs = {}
        else:
            hostname = socket.gethostname()
            # getting the IP address using socket.gethostbyname() method
            ip_address = socket.gethostbyname(hostname)
            file = __file__.replace("\\", "\\\\")
            err = traceback.format_exc().replace("\\", "\\\\")
            query = f"""
                mutation {{ addError(inputError: {{
                        errorTitle: "Getting Nap Checker Fades",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                    """
            print(query)

            result = requests.post(API_URL, headers=HEADERS, json={
                "query": query})
        return False
    except:
        hostname = socket.gethostname()
        # getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\", "\\\\")
        err = traceback.format_exc().replace("\\", "\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting Nap Checker Fades",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{__file__}",
                        err: "{traceback.format_exec()}",
                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})
        return False
