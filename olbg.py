import json
from pprint import pprint
import selenium
from dotenv import load_dotenv
import re
from time import sleep
from datetime import datetime, date, timedelta
import random
from gql import gql
import pytz
import traceback
import requests
import socket
from dotenv import load_dotenv
import traceback
import requests
import socket
import os
dotenv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env')
load_dotenv(dotenv_path)
PROXY = os.environ.get("PROXY")
API_URL = os.environ.get("API_URL")
API_KEY = os.environ.get("API_KEY")

HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
}

tipsters = [{
    "name": "Mannja99",
    "url": "https://www.olbg.com/best-tipsters/Horse_Racing/2/466120",

}, {
    "name": "Bravestes",
    "url": "https://www.olbg.com/best-tipsters/Horse_Racing/2/499840",

}, {
    "name": "The prophet",
    "url": "https://www.olbg.com/best-tipsters/Horse_Racing/2/114000",

}, {
    "name": "Seymour Winners",
    "url": "https://www.olbg.com/best-tipsters/Horse_Racing/2/570898",

}, {
    "name": "Edwinp",
    "url": "https://www.olbg.com/best-tipsters/Horse_Racing/2/535271",

}, {
    "name": "Uncle Fiddler",
    "url": "https://www.olbg.com/best-tipsters/Horse_Racing/2/508353",

}, {
    "name": "Neilboyfield",
    "url": "https://www.olbg.com/best-tipsters/Horse_Racing/2/603286",

}]


def olbg(self, task, url=None):

    def switchToTab():
        print("switching tab")
        self.drivers[self.tabs['olbg']['driver']
                     ]['driver'].switch_to.window(self.tabs['olbg']['handle'])
        refreshChecker = random.randint(1, 100)
        if refreshChecker >= 94:
            self.drivers[self.tabs['olbg']['driver']
                         ]['driver'].refresh()
            sleep(2)

    # get headers
    try:
        if task == "get_urls":
            return tipsters

        elif task == "get_data":
            print(url)
            tipster = url
            print(tipster)
            if "olbg" not in self.tabs:
                print("opening")
                self.openURL(
                    "olbg", "https://www.olbg.com/betting-tips", "standard")
            else:
                switchToTab()

            if 'olbg_trades' not in self.state:
                self.state['olbg_trades'] = {}

            todays_date_string = date.today().strftime("%Y-%m-%d")

            while True:
                try:
                    self.drivers[self.tabs['olbg']['driver']
                                 ]['driver'].get(tipster['url'])
                    break
                except selenium.common.exceptions.TimeoutException:
                    sleep(2)
                    continue

            sleep(2)
            html = self.drivers[self.tabs['olbg']['driver']
                                ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

            # turn page into soup
            main_soup = self.makeSoup(html)
            try:
                tips = main_soup.find('table', {'id': 'tipsterListingContainer'}).find(
                    "tbody").find_all("tr")
            except AttributeError:
                return

            print(tips)
            # loop through races
            skip = False

            for tip in tips:
                print(tip['class'])

                if 'sport-tips-table-comments-row' not in tip['class']:
                    if tip.find("div", class_="odds-betting-btn-holder").find("span").text == "NR":
                        skip = True
                        continue
                    tip_data = {
                        'url': tip.find("a", class_="selection-event-link")['href'],
                        'selection': tip.find("h4", class_="selection-name").text,
                        'course': tip.find("a", class_="selection-event-link").text.split(" ", 1)[1],
                        'time': tip.find("a", class_="selection-event-link").text.split(" ", 1)[0],
                        'odds': float(tip.find("div", class_="odds-betting-btn-holder").find("span").text),

                    }
                else:
                    if skip is False:
                        tip_data["tipType"] = "WIN" if "WIN" in tip.find(
                            "span", class_="sport-tips-table-comments-status").text else "EW"
                    else:
                        skip = False
                        continue

                    pprint(tip_data)
                    naive = datetime.now()
                    timezone = pytz.timezone("Europe/London")
                    aware1 = timezone.localize(naive)
                    try:
                        starttime = int(datetime.strptime(
                            f"{todays_date_string} 0{tip_data['time']} PM", "%Y-%m-%d %I:%M %p").timestamp()) - int(aware1.utcoffset().total_seconds())
                    except ValueError:
                        continue
                    print(
                        f'{tip_data["course"]}-{tip_data["selection"]}' not in self.state['olbg_trades'])
                    if f'{tip_data["course"]}-{tip_data["selection"]}' not in self.state['olbg_trades']:
                        trade_query = f"""
                            mutation {{newTrade(inputTradeAlert: {{
                                strategy: "{tip_data["tipType"]}",
                                selection: "{tip_data["selection"]}",
                                sport: "Horse Racing",
                                positionType: "Back",
                                event: "{tip_data["course"]}",
                                starttime: {starttime},
                                odds: {tip_data["odds"]}
                            }})}}
                        """
                        result = self.sendToApi(trade_query)
                        print(result)
                        if "errors" not in result:
                            self.state['olbg_trades'][f'{tip_data["course"]}-{tip_data["selection"]}'] = {
                                "strategy": tip_data["tipType"],
                                "selection": tip_data["selection"],
                                "sport": "Horse Racing",
                                "positionType": "Back",
                                "event": tip_data["course"],
                                "starttime": starttime,
                                "odds": tip_data["odds"]
                            }

            sleep(4)
            keys_to_delete = []
            for key, value in self.state['olbg_trades'].items():
                if value['starttime'] + 86400 < int((datetime.now() - timedelta(hours=1)).timestamp()):
                    keys_to_delete.append(key)
            for k in keys_to_delete:
                del self.state['olbg_trades'][k]
            self.saveState()

        # send to api
    except selenium.common.exceptions.WebDriverException:
        err = traceback.format_exc()
        if "disconnected from renderer" in err:
            self.quit()
            self.loadChromedriver(
                key='standard')
            self.loadChromedriver(key='proxyDriver', opts={
                'proxy': PROXY, 'vanila': True})
            self.tabs = {}
        else:
            hostname = socket.gethostname()
            # getting the IP address using socket.gethostbyname() method
            ip_address = socket.gethostbyname(hostname)
            file = __file__.replace("\\", "\\\\")
            err = traceback.format_exc().replace("\\", "\\\\")
            query = f"""
                        mutation {{ addError(inputError: {{
                            errorTitle: "Getting OBLG Horse Tips",
                            machine: "{ip_address}",
                            machineName: "API",
                            errorFileName: "{file}",
                            err: "{err},
                            critical: true
                        }})}}
                    """
            print(query)

            result = requests.post(API_URL, headers=HEADERS, json={
                "query": query})
        return False
    except:
        hostname = socket.gethostname()
        # getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\", "\\\\")
        err = traceback.format_exc().replace("\\", "\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting OBLG Horse Tips",
                        machine: "{ip_address}",
                        machineName: "API",

                        errorFileName: "{file}",
                        err: "{err},

                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})
        return False
