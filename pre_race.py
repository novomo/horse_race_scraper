from dotenv import load_dotenv
import re
from time import sleep
from datetime import datetime, date, timedelta
import random
import pytz

from dotenv import load_dotenv
import traceback
import requests
import socket
import os
dotenv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env')
load_dotenv(dotenv_path)
PROXY = os.environ.get("PROXY")
API_URL = os.environ.get("API_URL")
API_KEY = os.environ.get("API_KEY")

HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
}


def pre_race(self, task, url=None):

    def switchToTab():
        print("switching tab")
        self.drivers[self.tabs['racing_tv']['driver']
                     ]['driver'].switch_to.window(self.tabs['racing_tv']['handle'])
        refreshChecker = random.randint(1, 100)
        if refreshChecker >= 94:
            self.drivers[self.tabs['racing_tv']['driver']
                         ]['driver'].refresh()
            sleep(2)

    # get headers
    try:
        if task == "get_urls":
            if "racing_tv" not in self.tabs:
                print("opening")
                self.openURL(
                    "racing_tv", "https://www.racingtv.com/racecards", "standard")
            else:
                switchToTab()
            self.drivers[self.tabs['racing_tv']['driver']
                         ]['driver'].get("https://www.racingtv.com/racecards")

            html = self.drivers[self.tabs['racing_tv']['driver']
                                ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

            # turn page into soup
            main_soup = self.makeSoup(html)

            race_links = main_soup.find_all(
                'a', class_='race-selector__times__race')
            urls = []
            for race in race_links:
                # print(race['href'])
                urls.append(f"https://www.racingtv.com{race['href']}")

            return urls

        elif task == "get_data":

            if 'pre_race_trades' not in self.state:
                self.state['pre_race_trades'] = {}

            if "racing_tv" not in self.tabs:
                print("opening")
                self.openURL(
                    "racing_tv", "https://www.racingtv.com/racecards", "standard")
            else:
                switchToTab()
            # print(race_links)
            # loop through races

            todays_date_string = (
                date.today() + timedelta(days=1)).strftime("%Y-%m-%d")

            course = ""
            sprint_ground = ""
            round_ground = ""

            # print(race['href'])
            race_data = {'url': url}
            self.drivers[self.tabs['racing_tv']['driver']
                         ]['driver'].get(race_data['url'])
            html = self.drivers[self.tabs['racing_tv']['driver']
                                ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

            # turn page into soup
            race_soup = self.makeSoup(html)
            starttime_text = race_soup.find(
                'a', class_='tabs__item--active').text
            naive = datetime.now()
            timezone = pytz.timezone("Europe/London")
            aware1 = timezone.localize(naive)
            starttime = datetime.strptime(
                f"{todays_date_string} {starttime_text}", "%Y-%m-%d %H:%M")

            race_data['starttime'] = int(
                starttime.timestamp()) - int(aware1.utcoffset().total_seconds())

            track_data = race_soup.find_all('div', class_='race__subtitle')

            if len(track_data) < 2 or track_data[0].text == "" or track_data[1].text == "":
                return

            # May have issue if only one course
            # print(race_data['url'].split("/"))
            course = race_data['url'].split(
                "/")[4].replace("-", " ").capitalize()

            if ":" not in track_data[0].text:
                sprint_ground = track_data[0].text
                round_ground = track_data[0].text
            else:
                sprint_ground = track_data[0].text.split(":")[1].split(",")[0]
                try:
                    round_ground = track_data[0].text.split(",")[
                        1].split(":")[1]
                except IndexError:
                    round_ground = ""

            # print(track_data[1].text)
            dist = track_data[1].text.split(" ")

            if "m" in dist[1].lower():
                distance = float(f"{int(dist[1].lower().replace('m', ''))}")
                if re.search("\df", dist[2].lower()):
                    distance = distance + \
                        (1/8)*float(dist[2].lower().replace('f', ''))

            else:
                distance = (1/8)*float(dist[1].lower().replace('f', ''))

            race_title = race_soup.find('div', class_='race__name').text

            if "Handicap (Qualifier)" in race_title:
                race_type = "Handicap (Qualifier)"
            elif "Maiden Stakes" in race_title:
                race_type = "Maiden Stakes"
            elif "Handicap Hurdle" in race_title:
                race_type = "Handicap Hurdle"
            elif "(Premier Handicap)" in race_title:
                race_type = "(Premier Handicap)"
            elif "Stakes (Listed)" in race_title:
                race_type = "Stakes (Listed)"
            elif "Handicap Chase" in race_title:
                race_type = "Handicap Chase"
            elif "Novices' Handicap Hurdle" in race_title:
                race_type = "Novices' Handicap Hurdle"
            elif "Novices' Hurdle" in race_title:
                race_type = "Novices' Hurdle"
            elif "Maiden" in race_title:
                race_type = "Maiden"
            elif "Stakes" in race_title:
                race_type = "Stakes"
            elif "Handicap" in race_title:
                race_type = "Handicap"
            elif "Hurdle" in race_title:
                race_type = "Hurdle"
            elif "Novice" in race_title:
                race_type = "Novice"
            else:
                race_type = "Stakes"

            race_data['course'] = {}
            race_data['course']['name'] = course
            race_data['course']['ground'] = sprint_ground if distance < 1.5 else round_ground
            race_data['course']['distance'] = distance
            race_data['course']['courseType'] = "Hurdle" if "Hurdle" in race_title else "Flat"
            race_data['course']['raceType'] = race_type

            # get runner data

            race_data['runners'] = []

            runners = race_soup.find_all('div', class_='racecard__runner')
            # print(runners)
            CDWinner = False
            for runner in runners[1:]:
                runner_data = {}
                runner_data['horse'] = {}
                runner_data['jockey'] = {}

                runner_name = runner.find(
                    'div', class_='racecard__runner__name').find('a')
                if runner_name is None:
                    break
                runner_wgt_age = runner.find(
                    'div', class_='racecard__runner__column--weight_age').find_all('div')
                # print(runner_wgt_age)
                runner_form_equip = runner.find(
                    'div', class_='racecard__runner__column--form_lr').find_all('div')
                # print(runner_form_equip)

                # print(runner.find('div', class_='racecard__runner__person'))
                jockey_link = runner.find(
                    'div', class_='racecard__runner__person').find_all("span")[1].find('a')
                gate_div = runner.find(
                    'div', class_='racecard__runner__barrier')

                pacemap_positions = runner.find_all(
                    'div', class_='pacemap__position')

                pace = 0
                position = 0
                for count, pacemap_position in enumerate(pacemap_positions):
                    # print(pacemap_position.find('div', class_='pacemap__position__probability'))
                    p = float(pacemap_position.find('div', class_='pacemap__position__probability')[
                        "style"].split(":")[1].replace(";", "").strip())
                    pace_predicted_div = pacemap_position.find_all(
                        'div', class_='pacemap__position__predicted')
                    if p > pace:
                        pace = p
                        position = count + position
                    if len(pace_predicted_div) > 0:
                        if p != pace and position > count:
                            position = position + 0.5
                        if p != pace and position < count:
                            position = position - 0.5

                badges = runner.find("div", class_="racecard__runner__comment__badges").find_all(
                    "span", class_="racecard__runner__badges")
                print(badges)
                for badge in badges:
                    print(badge.text.strip())
                    if "CD" in badge.text.strip():
                        CDWinner = True
                    else:
                        CDWinner = False
                if len(badges) == 0:
                    CDWinner = False
                runner_data['horse']['url'] = f"https://www.racingtv.com{runner_name['href']}"
                runner_data['horse']['name'] = runner_name.text.split("(")[
                    0].strip()
                runner_data['horse']['age'] = runner_wgt_age[1].text
                runner_data['horse']['weight'] = runner_wgt_age[0].text
                runner_data['horse']['form'] = runner_form_equip[0].text
                runner_data['horse']['runningPosition'] = position
                runner_data['jockey']['name'] = jockey_link.text
                runner_data['jockey']['url'] = f"https://www.racingtv.com{jockey_link['href']}"
                runner_data['gate'] = 0 if gate_div is None else gate_div.text.replace(
                    "(", "").replace(")", "")
                runner_data['equipment'] = runner_form_equip[1].text
                runner_data['CDWinner'] = CDWinner

                race_data['runners'].append(runner_data)
                print(runner_data['horse']['name'])
                print(CDWinner)
            # pprint(race_data)
            open_cd_winner_position = False
            runner_string = ""
            for r in race_data['runners']:
                if r['CDWinner'] is True:
                    open_cd_winner_position = True
                    cd_text = "true"
                else:
                    cd_text = "false"
                runner_string = runner_string + f'''
                    {{
                        horse: {{
                            age: {r['horse']['age']},
                            name: "{r['horse']['name']}",
                            weight: "{r['horse']['weight']}",
                            form: "{r['horse']['form']}",
                            runningPosition: "{r['horse']['runningPosition']}",
                            url: "{r['horse']['url']}",
                            CDWinner: {cd_text}
                        }},
                        jockey: {{
                            name: "{r['jockey']['name']}",
                            url: "{r['jockey']['url']}"
                        }},
                        gate: {int(r["gate"])},
                        equipment: "{r["equipment"]}"
                    }},'''
            query = f"""
                mutation {{
                    addHorseRaceEvent( inputHorseRace: {{
                        url: "{race_data['url']}",
                        starttime: {race_data['starttime']}
                        runners: [{runner_string}]
                        course: {{
                            distance: {race_data['course']['distance']},
                            raceType: "{race_data['course']['name']}",
                            ground: "{race_data['course']['ground']}",
                            name: "{race_data['course']['name']}",
                            courseType: "{race_data['course']['courseType']}"
                        }}
                    }} )
                }}
            """

            result = self.sendToApi(query)
            print(result)
            eventID = result['data']['addHorseRaceEvent']
            if open_cd_winner_position is True:
                for r in race_data['runners']:
                    if r['CDWinner'] is True and f"{race_data['course']['name']}-{r['horse']['name']}" not in self.state['pre_race_trades']:

                        trade_query = f"""
                            mutation {{ newTrade(inputTradeAlert: {{
                                eventID: "{eventID}",
                                event: "{race_data['course']['name']}",
                                strategy: "CDWinner",
                                starttime: {race_data['starttime']}
                                positionType: "Back",
                                selection: "{r['horse']['name']}",
                                sport: "Horse Racing"
                            }})}}
                        """
                        print(trade_query)
                        result = self.sendToApi(trade_query)
                        if "errors" not in result:
                            self.state['pre_race_trades'][f"{race_data['course']['name']}-{r['horse']['name']}"] = {
                                "strategy": "CDWinner",
                                "selection": r['horse']['name'],
                                "sport": "Horse Racing",
                                "positionType": "Back",
                                "event": race_data['course']['name'],
                                "starttime": race_data['starttime']
                            }
            keys_to_delete = []
            for key, value in self.state['pre_race_trades'].items():
                if value['starttime'] < int((datetime.now() - timedelta(hours=1)).timestamp()):
                    keys_to_delete.append(key)
            for k in keys_to_delete:
                del self.state['pre_race_trades'][k]
            self.saveState()

        # send to api
    except selenium.common.exceptions.WebDriverException:
        err = traceback.format_exc()
        if "disconnected from renderer" in err:
            self.quit()
            self.loadChromedriver(
                key='standard')
            self.loadChromedriver(key='proxyDriver', opts={
                'proxy': PROXY, 'vanila': True})
            self.tabs = {}
        else:
            hostname = socket.gethostname()
            # getting the IP address using socket.gethostbyname() method
            ip_address = socket.gethostbyname(hostname)
            file = __file__.replace("\\", "\\\\")
            err = traceback.format_exc().replace("\\", "\\\\")
            query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting Pre Race Hosre Data",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                    """
            print(query)

            result = requests.post(API_URL, headers=HEADERS, json={
                "query": query})
        return False
    except:
        hostname = socket.gethostname()
        # getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\", "\\\\")
        err = traceback.format_exc().replace("\\", "\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting Pre Race Hosre Data",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{traceback.format_exec()}",
                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})
        return False
