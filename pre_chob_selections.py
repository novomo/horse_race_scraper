from dotenv import load_dotenv
from time import sleep
from datetime import datetime, date
import random
import pytz
import selenium
from dotenv import load_dotenv
import traceback
import requests
import socket
import os
dotenv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env')
load_dotenv(dotenv_path)
hostname = socket.gethostname()
# getting the IP address using socket.gethostbyname() method
ip_address = socket.gethostbyname(hostname)
API_URL = os.environ.get("API_URL")
API_KEY = os.environ.get("API_KEY")
PROXY = os.environ.get("PROXY")
HEADERS = {
    'Content-Type': 'application/json',
    'authorization': f"API {API_KEY}",
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
}


def pre_chob_selections(self, task, url=None):

    def switchToTab():
        print("switching tab")
        try:
            self.drivers[self.tabs['pre_chob_selections']['driver']
                         ]['driver'].switch_to.window(self.tabs['pre_chob_selections']['handle'])
        except selenium.common.exceptions.NoSuchWindowException:
            self.openURL(
                "pre_chob_selections", "https://cyots.co.uk/drm_score_selections.asp", "standard")
    try:
        if task == "get_urls":
            return ['https://cyots.co.uk/drm_score_selections.asp']

        if task == "get_data":
            if "pre_chob" not in self.tabs:
                print("opening")
                self.openURL(
                    "pre_chob", "https://cyots.co.uk/drm_score_selections.asp", "standard")
            else:
                switchToTab()
            self.drivers[self.tabs['pre_chob']['driver']
                         ]['driver'].get(url)

            html = self.drivers[self.tabs['pre_chob']['driver']
                                ]['driver'].find_element(self.By.TAG_NAME, "body").get_attribute("innerHTML")

            # turn page into soup
            main_soup = self.makeSoup(html)

            table = main_soup.find('table')
            picks = table.find_all("tr")
            todays_date_string = date.today().strftime("%Y-%m-%d")
            for pick in picks[2:]:
                cells = pick.find_all("td")
                selection = cells[2].text.replace("<>", "'")
                naive = datetime.now()
                timezone = pytz.timezone("Europe/London")
                aware1 = timezone.localize(naive)

                starttime = int(datetime.strptime(
                    f"{todays_date_string} {cells[0].text}:00", "%Y-%m-%d %H:%M:%S").timestamp()) - int(aware1.utcoffset().total_seconds())
                print(starttime)
                trade_query = f"""
                            mutation {{newTrade(inputTradeAlert: {{
                                strategy: "CHOB",
                                selection: "{selection}",
                                sport: "Horse Racing",
                                positionType: "Back",
                                starttime: {starttime},
                            }})}}
                        """
                result = self.sendToApi(trade_query)
                print(result)
    except selenium.common.exceptions.WebDriverException:
        err = traceback.format_exc()
        if "disconnected from renderer" in err:
            self.quit()
            self.loadChromedriver(
                key='standard')
            self.loadChromedriver(key='proxyDriver', opts={
                'proxy': PROXY, 'vanila': True})
            self.tabs = {}
        else:
            hostname = socket.gethostname()
            # getting the IP address using socket.gethostbyname() method
            ip_address = socket.gethostbyname(hostname)
            file = __file__.replace("\\", "\\\\")
            err = traceback.format_exc().replace("\\", "\\\\")
            query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting Greyhound Results",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                    """
            print(query)

            result = requests.post(API_URL, headers=HEADERS, json={
                "query": query})
        return False

    except:
        hostname = socket.gethostname()
        # getting the IP address using socket.gethostbyname() method
        ip_address = socket.gethostbyname(hostname)
        file = __file__.replace("\\", "\\\\")
        err = traceback.format_exc().replace("\\", "\\\\")
        query = f"""
                    mutation {{ addError(inputError: {{
                        errorTitle: "Getting Greyhound Results",
                        machine: "{ip_address}",
                        machineName: "API",
                        errorFileName: "{file}",
                        err: "{err},
                        critical: true
                    }})}}
                """
        print(query)

        result = requests.post(API_URL, headers=HEADERS, json={
            "query": query})
        return False
